﻿using System;

namespace SquarePrinter
{
    class Program
    {
        private int UserInput()
        {
            Console.WriteLine("Write a positive whole number");
            return Int32.Parse(Console.ReadLine());
        }

        private void AddCharsToArray(int num)
        {
            string[,] array = new string[num, num];
            for (int i = array.GetLowerBound(0); i < array.GetLength(0); i++)
            {
                for (int y = array.GetLowerBound(1); y < array.GetLength(1); y++)
                {
                    // ADD STAR TO ALL COLUMNS IN FIRST AND LAST ROW
                    if (i == array.GetLowerBound(0) || i == array.GetUpperBound(0))
                        array[i, y] = "*";
                    // ADD SPACE TO ALL OTHER ROWS EXCEPT FIRST AND LAST COLUMN
                    else if (i > array.GetLowerBound(0) && i < array.GetUpperBound(0))
                    {
                        array[i, y] = " ";
                        array[i, 0] = "*";
                        array[i, array.GetUpperBound(0)] = "*";
                    }
                    // IF LENGTH IS 6 OR BIGGER OTHERWISE NO INNER SQUARE
                    if (array.GetLength(0) >= 6)
                    {
                        // ADD STARS TO ALL COLUMNS IN THIRD ROW FROM FIRST AND LAST ROW
                        if (i == array.GetLowerBound(0) + 2 || i == array.GetUpperBound(0) - 2)
                        {
                            array[i, y] = "*";
                            // ADD SPACE TO THE COLUMN BETWEEN INNER AND OUTER SQUARE
                            if (y > 0 && y < 2 || y > array.GetUpperBound(1) - 2 && y < array.GetUpperBound(1))
                                array[i, y] = " ";
                        }
                        // ADD STARS TO THE THIRD COLUMN FROM FIRST AND LAST COLUMN IN ALL OTHER ROWS
                        else if (i > array.GetLowerBound(0) + 2 && i < array.GetUpperBound(0) - 2)
                            if (y == array.GetLowerBound(0) + 2 || y == array.GetUpperBound(0) - 2)
                                array[i, y] = "*";
                    }
                }
            }
            Print(array);
        }

        private void Print(string[,] array)
        {
            // print to console
            for (int i = 0; i < array.GetLength(0); i++)
            {
                for (int y = 0; y < array.GetLength(1); y++)
                    Console.Write(array[i, y]);
                Console.WriteLine();
            }
        }

        public void Loop()
        {
            // Get user input
            int num;
            do
                num = UserInput();
            while (num < 0);
            AddCharsToArray(num);
        }

        static void Main(string[] args)
        {
            Program prog = new Program();
            prog.Loop();
        }
    }
}
